const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//REGISTRATION
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)

    })

    return newUser.save().then((user, error) => {
        if(error) {
            return false

        } else {
            return true
        }
    })
}

// LOGIN 

module.exports.loginUser = (reqBody => {
    return User.findOne({email: reqBody.email}).then(result => {

        if(result == null) {
            return false
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if(isPasswordCorrect) {
                return {access: auth.createAccessToken(result)}
            } else {
                return false
            }
        }
    })
})

// SET USER AS ADMIN

module.exports.setUserAsAdmin = (userId, newBody) => {
    return User.findById(userId).then((result) => {


        result.isAdmin = newBody.isAdmin
        return result.save().then((NewAdminStatus, sendErr) => {

            if(sendErr){
                console.log(sendErr)
                return false
            } else {
                return NewAdminStatus
            }
        })
    })
}



// USER CHECK OUT - CREATE ORDER
module.exports.orderProduct = async (data) => {

   
        let isUserUpdated = await User.findById(data.userId).then( user =>{
    
            user.orders.push({productId: data.productId});
    
            return user.save().then((user, error) => {
                if(error) {
                    return false
                } else {
                    return true
                }
            })
        })
    
    
        let isProductUpdated = await Product.findById(data.productId).then( product => {
           
            product.OrderedBy.push({userId: data.userId});
           
            return product.save().then((product, error) => {
                if(error){
                    return false
                } else {
                    return true
                }
            })
        })
    

            if(isUserUpdated && isProductUpdated) {
                return true
            } 
    
  
            else {
                return false
            }
    
    }

    // RETRIEVE AUTHENTICATED USER'S ORDERS
module.exports.getOrders = (reqParams) => {
    return User.findById(reqParams.data.orders).then(result => {
        return result;
    })

}