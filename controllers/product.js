const Product = require("../models/Product");
const auth = require("../auth");


// RETRIEVE ALL ACTIVE PRODUCTS
module.exports.getAllActive = () => {
    return Product.find({isActive: true}).then(result => {
        return result;
    })

}

// RETRIEVE SINGLE PRODUCT
module.exports.getProduct = (reqParams) => {
    return Product.findById(reqParams.productId).then(result => {
        return result;
    })
}

// CREATE PRODUCT
module.exports.createProduct = (reqBody) => {
    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    })
    

    return newProduct.save().then((product, error) => {
        if(error) {
            return false
        } else {
            return true
        }
    })
}

// UPDATE A PRODUCT
module.exports.updateProduct = (reqParams, reqBody) => {

    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

        return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
        .then((course, error) => {
            if(error){
                return false
            } else {
                return true
            }
        })

}

// ARCHIVE PRODUCT
module.exports.archiveProduct = (productId, newBody) => {
    return Product.findById(productId).then((result) => {


        result.isActive = newBody.isActive
        return result.save().then((NewActiveStatus, sendErr) => {

            if(sendErr){
                console.log(sendErr)
                return false
            } else {
                return NewActiveStatus
            }
        })
    })
}